package ru.ngwow.authserver.network;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.MessageEvent;
import ru.ngwow.authserver.packet.Packet;
import ru.ngwow.authserver.run.Run;
import ru.ngwow.authserver.settings.AuthSettings;

public class PacketQueue extends Thread {
    
    private static Queue<MessageEvent> queue = new ConcurrentLinkedQueue<>();
    public static ExecutorService packetHandler = 
            Executors.newFixedThreadPool(AuthSettings.getInt("thread.packethandler"));
    
    public static void add(MessageEvent e) {
        queue.add(e);
    }

    @Override
    public void run() {
        while(Run.running) {
            MessageEvent e = queue.poll();
            if (e != null) {
                Packet input = (Packet) e.getMessage();
                Channel channel = (Channel) e.getChannel();
                Runnable handler = new PacketHandler(input, channel);
                packetHandler.execute(handler);
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                }
            }
        }
    }
}
