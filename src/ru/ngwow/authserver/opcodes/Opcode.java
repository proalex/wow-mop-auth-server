package ru.ngwow.authserver.opcodes;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.authserver.constants.AuthState;
import ru.ngwow.authserver.exceptions.NoSuchOpcodeException;
import ru.ngwow.authserver.handlers.AuthHandler;
import ru.ngwow.authserver.packet.Packet;
import ru.ngwow.authserver.session.AuthSession;

public enum Opcode {

    CMD_AUTH_LOGON_CHALLENGE(0x00, AuthState.UNAUTHED) {
        @Override
        public void handle(AuthSession session, Packet input) {
            try {
                AuthHandler.logonChallenge(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                        new Object[]{
                            new Date(),
                            AuthHandler.class.getName(),
                            ex.getMessage()
                        });
                session.getChannel().close();
            }
        }
    },
    CMD_AUTH_LOGON_PROOF(0x01, AuthState.PROOF) {
        @Override
        public void handle(AuthSession session, Packet input) {
            try {
                AuthHandler.logonProof(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                        new Object[]{
                            new Date(),
                            AuthHandler.class.getName(),
                            ex.getMessage()
                        });
                session.getChannel().close();
            }
        }
    },
    CMD_AUTH_RECONNECT_CHALLENGE(0x02, AuthState.UNAUTHED) {
        @Override
        public void handle(AuthSession session, Packet input) {
            try {
                AuthHandler.logonChallenge(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                        new Object[]{
                            new Date(),
                            AuthHandler.class.getName(),
                            ex.getMessage()
                        });
                session.getChannel().close();
            }
        }
    },
    CMD_AUTH_RECONNECT_PROOF(0x03, AuthState.PROOF) {
        @Override
        public void handle(AuthSession session, Packet input) {
            try {
                AuthHandler.logonProof(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                        new Object[]{
                            new Date(),
                            AuthHandler.class.getName(),
                            ex.getMessage()
                        });
                session.getChannel().close();
            }
        }
    },
    CMD_REALM_LIST(0x10, AuthState.AUTHED) {
        @Override
        public void handle(AuthSession session, Packet input) {
            try {
                AuthHandler.realmList(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                        new Object[]{
                            new Date(),
                            AuthHandler.class.getName(),
                            ex.getMessage()
                        });
                session.getChannel().close();
            }
        }
    };
    private byte opcode;
    private AuthState state;
    private static final Logger log = Logger.getLogger(Opcode.class.getName());

    Opcode(int opcode, AuthState state) {
        this.opcode = (byte) opcode;
        this.state = state;
    }

    public byte getData() {
        return opcode;
    }
    
    public static boolean checkState(Opcode opcode, AuthSession session) {
        if (opcode == null) {
            throw new NullPointerException("opcode is null");
        }
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        return opcode.state == session.getAuthState();
    }

    public static Opcode getOpcode(byte data) throws NoSuchOpcodeException {
        for (Opcode current : Opcode.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchOpcodeException();
    }

    public abstract void handle(AuthSession session, Packet input);
}