package ru.ngwow.authserver.constants;

public enum AuthResult {
    
    WOW_SUCCESS                             (0x00),
    WOW_FAIL_BANNED                         (0x03),
    WOW_FAIL_UNKNOWN_ACCOUNT                (0x04),
    WOW_FAIL_INCORRECT_PASSWORD             (0x05),
    WOW_FAIL_ALREADY_ONLINE                 (0x06),
    WOW_FAIL_NO_TIME                        (0x07),
    WOW_FAIL_DB_BUSY                        (0x08),
    WOW_FAIL_VERSION_INVALID                (0x09),
    WOW_FAIL_VERSION_UPDATE                 (0x0A),
    WOW_FAIL_SUSPENDED                      (0x0C),
    WOW_SUCCESS_SURVEY                      (0x0E),
    WOW_FAIL_PARENTCONTROL                  (0x0F),
    WOW_FAIL_LOCKED_ENFORCED                (0x10),
    WOW_FAIL_TRIAL_ENDED                    (0x11),
    WOW_FAIL_USE_BATTLENET                  (0x12),
    WOW_FAIL_TOO_FAST                       (0x16),
    WOW_FAIL_CHARGEBACK                     (0x17),
    WOW_FAIL_GAME_ACCOUNT_LOCKED            (0x18),
    WOW_FAIL_INTERNET_GAME_ROOM_WITHOUT_BNET(0x19),
    WOW_FAIL_UNLOCKABLE_LOCK                (0x20);
    
    private byte result;
    
    AuthResult(int result) {
        this.result = (byte) result;
    }
    
    public byte getData() {
        return result;
    }
}
