package ru.ngwow.authserver.network;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

public class PacketEncoder extends OneToOneEncoder {

    @Override
    protected Object encode(ChannelHandlerContext channelhandlercontext, Channel channel, Object msg)
            throws Exception {
        return (ChannelBuffer) msg;
    }
}