package ru.ngwow.authserver.run;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.execution.OrderedMemoryAwareThreadPoolExecutor;
import ru.ngwow.authserver.database.helper.DBHelper;
import ru.ngwow.authserver.network.PacketQueue;
import ru.ngwow.authserver.network.ServerPipelineFactory;
import ru.ngwow.authserver.realmchecker.RealmChecker;
import ru.ngwow.authserver.settings.AuthSettings;

public class Run {

    private static final Logger log = Logger.getLogger(Run.class.getName());
    public static boolean running = true;

    public static void main(String[] args) throws IOException {
        ServerBootstrap bootstrap = null;
        Channel bind = null;
        try {
            System.out.println("NGWoW Auth Server");
            ChannelFactory factory;
            AuthSettings.load();
            ExecutorService bossExecutor =
                    new OrderedMemoryAwareThreadPoolExecutor
                    (AuthSettings.getInt("thread.acceptor"), 1048576, 1048576);
            ExecutorService ioExecutor =
                    new OrderedMemoryAwareThreadPoolExecutor
                    (AuthSettings.getInt("thread.io"), 1048576, 1048576);
            factory = new NioServerSocketChannelFactory
                    (bossExecutor, ioExecutor, AuthSettings.getInt("thread.io"));
            bootstrap = new ServerBootstrap(factory);
            bootstrap.setPipelineFactory(new ServerPipelineFactory());
            bootstrap.setOption("child.tcpNoDelay", true);
            bootstrap.setOption("child.keepAlive", true);
            DBHelper.updateOnline();
            log.log(Level.INFO, "Start listening {0} on {1} port...",
                    new Object[]{
                        AuthSettings.get("network.ip"),
                        AuthSettings.get("network.port")
                    });
            bind = bootstrap.bind(
                    new InetSocketAddress(
                    AuthSettings.get("network.ip"),
                    AuthSettings.getInt("network.port")));
            new Thread(new PacketQueue(), "PacketQueue").start();
            log.log(Level.INFO, "Starting realm checker...");
            new Thread(new RealmChecker(), "RealmChecker").start();
        } catch (Exception ex) {
            running = false;
            log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.",
                new Object[]{
                    new Date(),
                    Run.class.getName(),
                    ex.getMessage()
                });
            if (bind != null) {
                bind.close();
            }
            if (bootstrap != null) {
                bootstrap.shutdown();
            }
            System.exit(0);
        }
    }
}
