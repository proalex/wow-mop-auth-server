package ru.ngwow.authserver.network;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import ru.ngwow.authserver.packet.Packet;

public class PacketDecoder extends FrameDecoder {

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer)
            throws Exception {
        Packet packet = new Packet();
        packet.writeBytes(buffer.readBytes(buffer.readableBytes()).array());
        return packet;
    }
}
