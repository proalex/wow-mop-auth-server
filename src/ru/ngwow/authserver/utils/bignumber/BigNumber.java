package ru.ngwow.authserver.utils.bignumber;

import java.math.BigInteger;
import java.security.SecureRandom;

public class BigNumber {

    private BigInteger bigInteger;

    public BigNumber() {
        this.bigInteger = BigInteger.ZERO;
    }

    public BigNumber(final BigInteger bigInteger) {
        if (bigInteger == null) {
            throw new NullPointerException("bigInteger is null");
        }
        this.bigInteger = bigInteger.abs();
    }

    public BigNumber(final String str) {
        this.bigInteger = new BigInteger(str, 16);
    }

    public BigNumber(byte[] array) {
        if (array == null) {
            throw new NullPointerException("array is null");
        }
        if (array[0] < 0) {
            final byte[] tmp = new byte[array.length + 1];
            System.arraycopy(array, 0, tmp, 1, array.length);
            array = tmp;
        }
        this.bigInteger = new BigInteger(array);
    }

    public BigNumber add(final BigNumber val) {
        if (val == null) {
            throw new NullPointerException("val is null");
        }
        return new BigNumber(this.bigInteger.add(val.getBigInteger()));
    }

    public BigNumber multiply(final BigNumber val) {
        if (val == null) {
            throw new NullPointerException("val is null");
        }
        return new BigNumber(this.bigInteger.multiply(val.getBigInteger()));
    }

    public void setHexStr(final String str) {
        this.bigInteger = new BigInteger(str, 16);
    }

    public void setRand(final int numBytes) {
        final SecureRandom random = new SecureRandom();
        final byte[] array = random.generateSeed(numBytes);
        this.bigInteger = new BigInteger(1, array);
    }

    public void setBinary(byte[] array) {
        if (array == null) {
            throw new NullPointerException("array is null");
        }
        final int length = array.length;
        for (int i = 0; i < (length / 2); i++) {
            final byte j = array[i];
            array[i] = array[length - 1 - i];
            array[length - 1 - i] = j;
        }
        if (array[0] < 0) {
            final byte[] tmp = new byte[array.length + 1];
            System.arraycopy(array, 0, tmp, 1, array.length);
            array = tmp;
        }
        this.bigInteger = new BigInteger(array);
    }

    public BigNumber mod(final BigNumber m) {
        if (m == null) {
            throw new NullPointerException("m is null");
        }
        return new BigNumber(this.bigInteger.mod(m.getBigInteger()));
    }

    public BigNumber modPow(final BigNumber exponent, final BigNumber m) {
        if (exponent == null) {
            throw new NullPointerException("exponent is null");
        }
        if (m == null) {
            throw new NullPointerException("m is null");
        }
        return new BigNumber(this.bigInteger.modPow(exponent.getBigInteger(), m.getBigInteger()));
    }

    public byte[] asByteArray(final int minSize) {
        byte[] array = this.bigInteger.toByteArray();
        if (array[0] == 0) {
            final byte[] tmp = new byte[array.length - 1];
            System.arraycopy(array, 1, tmp, 0, tmp.length);
            array = tmp;
        }
        final int length = array.length;
        for (int i = 0; i < (length / 2); i++) {
            final byte j = array[i];
            array[i] = array[length - 1 - i];
            array[length - 1 - i] = j;
        }
        if (minSize > length) {
            final byte[] newArray = new byte[minSize];
            System.arraycopy(array, 0, newArray, minSize - length, length);
            return newArray;
        }
        return array;
    }

    public byte[] asByteArray() {
        byte[] array = this.bigInteger.toByteArray();
        if (array[0] == 0) {
            final byte[] tmp = new byte[array.length - 1];
            System.arraycopy(array, 1, tmp, 0, tmp.length);
            array = tmp;
        }
        final int length = array.length;
        for (int i = 0; i < (length / 2); i++) {
            final byte j = array[i];
            array[i] = array[length - 1 - i];
            array[length - 1 - i] = j;
        }
        return array;
    }

    public String asHexStr() {
        return this.bigInteger.toString(16).toUpperCase();
    }

    public BigInteger getBigInteger() {
        return this.bigInteger.abs();
    }

    public int compareTo(BigNumber number) {
        if (number == null) {
            throw new NullPointerException("number is null");
        }
        return this.bigInteger.compareTo(number.getBigInteger());
    }
}
