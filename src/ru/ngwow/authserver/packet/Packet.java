package ru.ngwow.authserver.packet;

import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferFactory;
import org.jboss.netty.buffer.HeapChannelBuffer;
import org.jboss.netty.buffer.HeapChannelBufferFactory;
import org.jboss.netty.buffer.LittleEndianHeapChannelBuffer;
import ru.ngwow.authserver.utils.uint.UInt16;
import ru.ngwow.authserver.utils.uint.UInt32;
import ru.ngwow.authserver.utils.uint.UInt64;
import ru.ngwow.authserver.utils.uint.UInt8;
import static ru.ngwow.authserver.utils.uint.Unsigned.*;
import ru.ngwow.authserver.opcodes.Opcode;

public class Packet extends HeapChannelBuffer {

    public Packet(int length) {
        super(length);
    }
    
    public Packet(int length, Opcode opcode) {
        super(length);
        if (opcode == null) {
            throw new NullPointerException("opcode is null");
        }
        writeUInt8(opcode.getData());
    }
    
    public Packet(Opcode opcode) {
        super(512);
        if (opcode == null) {
            throw new NullPointerException("opcode is null");
        }
        writeUInt8(opcode.getData());
    }
    
    public Packet() {
        super(512);
    }

    public Packet(byte[] array) {
        super(array);
    }

    private Packet(byte[] array, int readerIndex, int writerIndex) {
        super(array, readerIndex, writerIndex);
    }

    @Override
    public ChannelBufferFactory factory() {
        return HeapChannelBufferFactory.getInstance(ByteOrder.LITTLE_ENDIAN);
    }

    @Override
    public ByteOrder order() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public short getShort(int index) {
        return (short) (array[index] & 0xFF | array[index + 1] << 8);
    }

    @Override
    public int getUnsignedMedium(int index) {
        return array[index] & 0xff |
               (array[index + 1] & 0xff) <<  8 |
               (array[index + 2] & 0xff) << 16;
    }

    @Override
    public int getInt(int index) {
        return array[index] & 0xff |
               (array[index + 1] & 0xff) <<  8 |
               (array[index + 2] & 0xff) << 16 |
               (array[index + 3] & 0xff) << 24;
    }

    @Override
    public long getLong(int index) {
        return (long) array[index] & 0xff |
               ((long) array[index + 1] & 0xff) <<  8 |
               ((long) array[index + 2] & 0xff) << 16 |
               ((long) array[index + 3] & 0xff) << 24 |
               ((long) array[index + 4] & 0xff) << 32 |
               ((long) array[index + 5] & 0xff) << 40 |
               ((long) array[index + 6] & 0xff) << 48 |
               ((long) array[index + 7] & 0xff) << 56;
    }

    @Override
    public void setShort(int index, int value) {
        array[index]     = (byte) value;
        array[index + 1] = (byte) (value >>> 8);
    }

    @Override
    public void setMedium(int index, int   value) {
        array[index]     = (byte) value;
        array[index + 1] = (byte) (value >>> 8);
        array[index + 2] = (byte) (value >>> 16);
    }

    @Override
    public void setInt(int index, int   value) {
        array[index]     = (byte) value;
        array[index + 1] = (byte) (value >>> 8);
        array[index + 2] = (byte) (value >>> 16);
        array[index + 3] = (byte) (value >>> 24);
    }

    @Override
    public void setLong(int index, long  value) {
        array[index]     = (byte) value;
        array[index + 1] = (byte) (value >>> 8);
        array[index + 2] = (byte) (value >>> 16);
        array[index + 3] = (byte) (value >>> 24);
        array[index + 4] = (byte) (value >>> 32);
        array[index + 5] = (byte) (value >>> 40);
        array[index + 6] = (byte) (value >>> 48);
        array[index + 7] = (byte) (value >>> 56);
    }

    @Override
    public ChannelBuffer duplicate() {
        return new Packet(array, readerIndex(), writerIndex());
    }
    

    @Override
    public ChannelBuffer copy(int index, int length) {
        if (index < 0 || length < 0 || index + length > array.length) {
            throw new IndexOutOfBoundsException("Too many bytes to copy - Need "
                    + (index + length) + ", maximum is " + array.length);
        }

        byte[] copiedArray = new byte[length];
        System.arraycopy(array, index, copiedArray, 0, length);
        return new LittleEndianHeapChannelBuffer(copiedArray);
    }
    
    public UInt8 readUInt8() {
        return uint8(readByte());
    }
    
    public UInt16 readUInt16() {
        return uint16(readShort());
    }
    
    public UInt32 readUInt32() {
        return uint32(readInt());
    }
    
    public UInt64 readUInt64() {
        return uint64(readLong());
    }
    
    public byte readInt8() {
        return readByte();
    }
    
    public short readInt16() {
        return readShort();
    }
    
    public int readInt32() {
        return readInt();
    }
    
    public long readInt64() {
        return readLong();
    }
    
    public String readString(int size) {
        byte[] data = new byte[size];
        readBytes(data);
        return new String(data, Charset.forName("UTF-8"));
    }
    
    public void writeUInt8(UInt8 uint8) {
        if (uint8 == null) {
            throw new NullPointerException("uint8 is null");
        }
        writeByte(uint8.byteValue());
    }
    
    public void writeUInt16(UInt16 uint16) {
        if (uint16 == null) {
            throw new NullPointerException("uint16 is null");
        }
        writeShort(uint16.shortValue());
    }
    
    public void writeUInt32(UInt32 uint32) {
        if (uint32 == null) {
            throw new NullPointerException("uint32 is null");
        }
        writeInt(uint32.intValue());
    }
    
    public void writeUInt64(UInt64 uint64) {
        if (uint64 == null) {
            throw new NullPointerException("uint64 is null");
        }
        writeLong(uint64.longValue());
    }
    
    public void writeUInt8(long uint8) {
        writeByte(uint8((byte) uint8).byteValue());
    }
    
    public void writeUInt16(long uint16) {
        writeShort(uint16((short) uint16).shortValue());
    }
    
    public void writeUInt32(long uint32) {
        writeInt(uint32((int) uint32).intValue());
    }
    
    public void writeUInt64(long uint64) {
        writeLong(uint64(uint64).longValue());
    }
    
    public void writeInt8(long int8) {
        writeByte((byte) int8);
    }
    
    public void writeInt16(long int16) {
        writeShort((short) int16);
    }
    
    public void writeInt32(long int32) {
        writeInt((int) int32);
    }
    
    public void writeInt64(long int64) {
        writeLong((long) int64);
    }
    
    public void writeString(String data) {
        if (data == null) {
            throw new NullPointerException("data is null");
        }
        writeBytes(data.getBytes(Charset.forName("UTF-8")));
    }
    
    public void writePackedTime() {
        Calendar calendar = GregorianCalendar.getInstance();
        writeInt32((calendar.get(Calendar.YEAR) - 100) << 24 |
                calendar.get(Calendar.MONTH) << 20 |
                (calendar.get(Calendar.DAY_OF_MONTH) - 1) << 14 |
                calendar.get(Calendar.DAY_OF_WEEK) << 11 |
                calendar.get(Calendar.HOUR) << 6 |
                calendar.get(Calendar.MINUTE));
    }
    
    public void writeGuid(long guid) {
        byte[] packetGuid = new byte[9];
        int length = 1;
        for (int i = 0; guid != 0; i++) {
            if ((guid & 0xFF) != 0) {
                packetGuid[0] |= (byte)(1 << i);
                packetGuid[length] = (byte)(guid & 0xFF);
                length++;
            }
            guid >>>= 8;
        }
        writeBytes(packetGuid, 0, length);
    }
}
