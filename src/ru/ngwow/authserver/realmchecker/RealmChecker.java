package ru.ngwow.authserver.realmchecker;

import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ru.ngwow.authserver.database.MySqlConnection;
import ru.ngwow.authserver.database.query.Query;
import ru.ngwow.authserver.run.Run;

public class RealmChecker extends Thread {
    
    @Override
    public void run() {
        Object current = null;
        MySqlConnection dbConnection = null;
        while (Run.running) {
            try {
                ResultSet realmList;
                boolean failed = false;
                dbConnection = MySqlConnection.getInstance();
                current = dbConnection.getAuthPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getRealms);
                realmList = statment.executeQuery();
                while (realmList.next()) {
                    String host = realmList.getString("address");
                    int port = realmList.getInt("port");
                    Socket sock = null;
                    try {
                        sock = new Socket(host, port);
                    } catch (Exception ex) {
                        failed = true;
                    }
                    if (!failed && sock.isConnected()) {
                        statment = connection.prepareStatement(Query.updateRealm);
                        statment.setInt(1, 0);
                        statment.setInt(2, realmList.getInt("id"));
                        statment.execute();
                        sock.close();
                    } else {
                        statment = connection.prepareStatement(Query.updateRealm);
                        statment.setInt(1, 2);
                        statment.setInt(2, realmList.getInt("id"));
                        statment.execute();
                    }
                }
                sleep(1000);
            } catch (Exception ex) {
            } finally {
            if (current != null) {
                try {
                    dbConnection.getAuthPool().returnObject(current);
                } catch (Exception ex) {
                }
            }
            }
        }
    }
}
