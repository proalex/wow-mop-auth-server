package ru.ngwow.authserver.database.helper;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.authserver.database.MySqlConnection;
import ru.ngwow.authserver.database.query.Query;


public class DBHelper {
    
    private static final Logger log = Logger.getLogger(DBHelper.class.getName());
    
    private DBHelper() {
    }
    
    public static void updateOnline() throws IOException, Exception {
        log.log(Level.INFO, "Cleaning expired bans...");
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.cleanBans);
            statment.execute();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
    }
}
