package ru.ngwow.authserver.network;

import org.jboss.netty.channel.Channel;
import ru.ngwow.authserver.exceptions.NoSuchOpcodeException;
import ru.ngwow.authserver.opcodes.Opcode;
import ru.ngwow.authserver.packet.Packet;
import ru.ngwow.authserver.session.AuthSession;

public class PacketHandler implements Runnable{

    private Packet input;
    private Channel channel;
    
    public PacketHandler(Packet input, Channel channel) {
        this.input = input;
        this.channel = channel;
    }
    
    @Override
    public void run() {
        Opcode opcode = null;
        byte data = input.readInt8();
        AuthSession session = (AuthSession) channel.getAttachment();
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        try {
            opcode = Opcode.getOpcode(data);
            if (Opcode.checkState(opcode, session)) {
                opcode.handle(session, input);
            }
        } catch (NoSuchOpcodeException ex) {
            channel.close();
        }
    }
}
