package ru.ngwow.authserver.database.query;

public interface Query {

    static String getAccountInfo = "SELECT id, password, sessionkey, online FROM"
            + " account WHERE email = ?";
    static String getAccountSessionKey = "SELECT sessionkey FROM account WHERE"
            + " email = ?";
    static String setSessionKey = "UPDATE account SET sessionkey = ? WHERE id = ?";
    static String getRealms = "SELECT id, name, address, port, icon, flag,"
            + " timezone, population FROM realms";
    static String getCharactersCount = "SELECT count FROM characters"
            + " WHERE realmid = ? AND accountid = ?";
    static String getBannedState = "SELECT COUNT(id) FROM account_banned WHERE"
            + " id = ? AND (unbandate > UNIX_TIMESTAMP() OR unbandate = 0)";
    static String cleanBans = "DELETE FROM account_banned WHERE unbandate < UNIX_TIMESTAMP()";
    static String updateRealm = "UPDATE realms SET flag = ? WHERE id = ?";
}
