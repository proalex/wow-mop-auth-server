package ru.ngwow.authserver.handlers;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.jboss.netty.channel.Channel;
import ru.ngwow.authserver.constants.AuthCmd;
import ru.ngwow.authserver.constants.AuthResult;
import ru.ngwow.authserver.constants.AuthState;
import ru.ngwow.authserver.database.MySqlConnection;
import ru.ngwow.authserver.database.query.Query;
import ru.ngwow.authserver.opcodes.Opcode;
import ru.ngwow.authserver.packet.Packet;
import ru.ngwow.authserver.session.AuthSession;
import ru.ngwow.authserver.settings.AuthSettings;
import ru.ngwow.authserver.utils.uint.UInt16;
import ru.ngwow.authserver.utils.bignumber.BigNumber;

public final class AuthHandler {

    private static final BigNumber g = new BigNumber("7");
    private static final BigNumber n = new BigNumber("894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7");
    private static final BigNumber k = new BigNumber("3");

    private AuthHandler() {
    }

    public static void logonChallenge(AuthSession session, Packet input)
            throws NoSuchAlgorithmException, Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Packet output = new Packet(Opcode.CMD_AUTH_LOGON_CHALLENGE);
        Channel channel = session.getChannel();
        String ip = channel.getRemoteAddress().toString();
        output.writeUInt8(AuthCmd.CMD_GRUNT_AUTH_CHALLENGE.getData());
        input.skipBytes(10);
        UInt16 clientBuild = input.readUInt16();
        int test = clientBuild.intValue();
        if (clientBuild.intValue() != AuthSettings.getInt("gamebuild")) {
            output.writeUInt8(AuthResult.WOW_FAIL_UNKNOWN_ACCOUNT.getData());
            channel.write(output);
            return;
        }
        input.skipBytes(20);
        short loginLength = input.readUnsignedByte();
        String email = input.readString(loginLength);
        if (!session.getInfo(email)) {
            output.writeUInt8(AuthResult.WOW_FAIL_UNKNOWN_ACCOUNT.getData());
            channel.write(output);
            return;
        }
        if (session.isBanned()) {
            output.writeUInt8(AuthResult.WOW_FAIL_GAME_ACCOUNT_LOCKED.getData());
            channel.write(output);
            return;
        } else if (session.isOnline()) {
            output.writeUInt8(AuthResult.WOW_FAIL_ALREADY_ONLINE.getData());
            channel.write(output);
            return;
        }
        session.initVSFields();
        output.writeByte(AuthResult.WOW_SUCCESS.getData());
        BigNumber b = new BigNumber();
        b.setRand(19);
        session.setb(b);
        BigNumber gmod = g.modPow(b, n);
        session.setB(((session.getV().multiply(k)).add(gmod)).mod(n));
        BigNumber unk = new BigNumber();
        unk.setRand(16);
        output.writeBytes(session.getB().asByteArray(32));
        output.writeUInt8(0x01);
        output.writeBytes(g.asByteArray(1));
        output.writeUInt8(0x20);
        output.writeBytes(n.asByteArray(32));
        output.writeBytes(session.getS().asByteArray(32));
        output.writeBytes(unk.asByteArray(16));
        output.writeUInt8(0);
        session.setAuthState(AuthState.PROOF);
        channel.write(output);
    }

    public static void logonProof(AuthSession session, Packet input)
            throws NoSuchAlgorithmException, Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        if (session.isBanned()) {
            session.getChannel().close();
            return;
        }
        Channel channel = session.getChannel();
        byte[] aBinary = new byte[32];
        input.readBytes(aBinary);
        byte[] m1Binary = new byte[20];
        input.readBytes(m1Binary);
        BigNumber m1 = new BigNumber();
        m1.setBinary(m1Binary);
        BigNumber a = new BigNumber();
        a.setBinary(aBinary);
        if (a.compareTo(new BigNumber("0")) == 0) {
            session.getChannel().close();
            return;
        }
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        sha.update(a.asByteArray());
        sha.update(session.getB().asByteArray());
        BigNumber u = new BigNumber();
        u.setBinary(sha.digest());
        BigNumber s = (a.multiply(session.getV().modPow(u, n))).modPow(session.getb(), n);
        byte[] t = s.asByteArray(32);
        byte[] t1 = new byte[16];
        byte[] vK = new byte[40];
        for (int i = 0; i < 16; ++i) {
            t1[i] = t[i * 2];
        }
        sha.update(t1);
        byte[] t2 = sha.digest();
        for (int i = 0; i < 20; ++i) {
            vK[i * 2] = t2[i];
        }
        for (int i = 0; i < 16; ++i) {
            t1[i] = t[(i * 2) + 1];
        }
        sha.update(t1);
        t2 = sha.digest();
        for (int i = 0; i < 20; ++i) {
            vK[(i * 2) + 1] = t2[i];
        }
        BigNumber k = new BigNumber();
        k.setBinary(vK);
        sha.update(n.asByteArray());
        byte[] hash = sha.digest();
        sha.update(g.asByteArray());
        byte[] gHash = sha.digest();
        for (int i = 0; i < 20; ++i) {
            hash[i] ^= gHash[i];
        }
        sha.update(session.getEmail().getBytes(Charset.forName("UTF-8")));
        byte[] t4 = sha.digest();
        sha.update(hash);
        sha.update(t4);
        sha.update(session.getS().asByteArray());
        sha.update(a.asByteArray());
        sha.update(session.getB().asByteArray());
        sha.update(k.asByteArray());
        BigNumber m = new BigNumber();
        m.setBinary(sha.digest());
        sha.update(a.asByteArray());
        sha.update(m.asByteArray());
        sha.update(k.asByteArray());
        if (m.compareTo(m1) == 0) {
            session.setSessionKey(k);
            session.setAuthed(true);
            Packet output = new Packet(Opcode.CMD_AUTH_LOGON_PROOF);
            output.writeUInt8(AuthResult.WOW_SUCCESS.getData());
            output.writeBytes(sha.digest());
            output.writeUInt32(0x800000);
            output.writeUInt32(0);
            output.writeUInt16(0);
            channel.write(output);
            session.setAuthState(AuthState.AUTHED);
        } else {
            Packet output = new Packet(Opcode.CMD_AUTH_LOGON_PROOF);
            output.writeUInt8(AuthResult.WOW_FAIL_UNKNOWN_ACCOUNT.getData());
            output.writeUInt8(3);
            output.writeUInt8(0);
            channel.write(output);
        }
    }

    public static void realmList(AuthSession session, Packet input)
            throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        if (!session.isAuthed()) {
            session.getChannel().close();
            return;
        }
        Packet output = new Packet(Opcode.CMD_REALM_LIST);
        Packet realms = new Packet();
        Packet realmsSize = new Packet();
        Channel channel = session.getChannel();
        ResultSet realmList;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getRealms);
            realmList = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        int realmCount = 0;
        while (realmList.next()) {
            int characters = 0;
            ResultSet realmCharacters;
            current = null;
            try {
                current = dbConnection.getAuthPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharactersCount);
                statment.setInt(1, realmList.getInt("id"));
                statment.setInt(2, session.getId());
                realmCharacters = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getAuthPool().returnObject(current);
                }
            }
            if (realmCharacters.next()) {
                characters = realmCharacters.getInt("count");
            }
            realms.writeUInt8(realmList.getInt("icon"));
            realms.writeUInt8(0);
            realms.writeUInt8(realmList.getInt("flag"));
            realms.writeString(realmList.getString("name"));
            realms.writeUInt8(0);
            realms.writeString(realmList.getString("address"));
            realms.writeBytes(":".getBytes("UTF-8"));
            realms.writeString(realmList.getString("port"));
            realms.writeUInt8(0);
            realms.writeFloat(realmList.getFloat("population"));
            realms.writeUInt8(characters);
            realms.writeUInt8(realmList.getInt("timezone"));
            realms.writeUInt8(0x2C);
            realmCount++;
        }
        realms.writeUInt8(0x10);
        realms.writeUInt8(0);
        realmsSize.writeUInt32(0);
        realmsSize.writeUInt16(realmCount);
        int dataSize = realms.readableBytes() + realmsSize.readableBytes();
        output.writeShort(dataSize);
        output.writeBytes(realmsSize);
        output.writeBytes(realms);
        channel.write(output);
    }
}
