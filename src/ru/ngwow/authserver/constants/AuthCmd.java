package ru.ngwow.authserver.constants;

public enum AuthCmd {

    CMD_GRUNT_AUTH_CHALLENGE(0x00),
    CMD_GRUNT_AUTH_VERIFY   (0x02),
    CMD_GRUNT_CONN_PING     (0x10),
    CMD_GRUNT_CONN_PONG     (0x11),
    CMD_GRUNT_HELLO         (0x20),
    CMD_GRUNT_PROVESESSION  (0x21),
    CMD_GRUNT_KICK          (0x24),
    CMD_GRUNT_PCWARNING     (0x29),
    CMD_GRUNT_STRINGS       (0x41),
    CMD_GRUNT_SUNKENUPDATE  (0x44),
    CMD_GRUNT_SUNKEN_ONLINE (0x46),
    CMD_GRUNT_CAISTIMEUPDATE(0x2C);
    
    private byte cmd;
    
    AuthCmd(int cmd) {
        this.cmd = (byte) cmd;
    }
    
    public byte getData() {
        return cmd;
    }
}
