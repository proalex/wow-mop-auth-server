package ru.ngwow.authserver.utils.variabletype;

public enum VariableType {
    
    STRING("java.lang.String"),
    UINT32("ru.ngwow.authserver.utils.uint.UInt32"),
    FLOAT("java.lang.Float"),
    FLOAT_ARRAY("java.lang.Float[]"),
    INT32("java.lang.Integer"),
    BYTE("java.lang.Byte"),
    UINT16("ru.ngwow.authserver.utils.uint.UInt16"),
    UINT64("ru.ngwow.authserver.utils.uint.UInt64");
    
    private String typeValue;
    
    private VariableType(String type) {
        if (type == null) {
            throw new NullPointerException("type is null");
        }
        typeValue = type;
    }
    
    static public VariableType getType(String pType) {
        if (pType == null) {
            throw new NullPointerException("pType is null");
        }
        for (VariableType type: VariableType.values()) {
            if (type.getTypeValue().equals(pType)) {
                return type;
            }
        }
        throw new RuntimeException("unknown type");
    }
    
    public String getTypeValue() {
        return typeValue;
    }
}
