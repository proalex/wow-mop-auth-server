package ru.ngwow.authserver.constants;

public enum AuthState {
    
    UNAUTHED,
    PROOF,
    AUTHED
}
