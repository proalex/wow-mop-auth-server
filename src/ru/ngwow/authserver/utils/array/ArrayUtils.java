package ru.ngwow.authserver.utils.array;

public class ArrayUtils {

    private ArrayUtils() {
    }

    public static void reverse(byte[] a) {
        if (a == null) {
            throw new NullPointerException("a is null");
        }
        for (int i = 0; i < a.length / 2; i++) {
            byte temp = a[i];
            a[i] = a[a.length - i - 1];
            a[a.length - i - 1] = temp;
        }
    }
    
    public static int byteArrayToInt(byte[] a) {
        if (a == null) {
            throw new NullPointerException("a is null");
        }
        return (a[0] & 0xFF)
                | (a[1] & 0xFF) << 8
                | (a[2] & 0xFF) << 16
                | (a[3] & 0xFF) << 24;
    }

    public static long byteArrayToLong(byte[] a) {
        if (a == null) {
            throw new NullPointerException("a is null");
        }
        return (a[0] & 0xFF)
                | (a[1] & 0xFF) << 8
                | (a[2] & 0xFF) << 16
                | (a[3] & 0xFF) << 24
                | (long)(a[4] & 0xFF) << 32
                | (long)(a[5] & 0xFF) << 40
                | (long)(a[6] & 0xFF) << 48
                | (long)(a[7] & 0xFF) << 56;
    }

    public static byte[] intToByteArray(int i) {
        return new byte[]{
                    (byte) (i),
                    (byte) ((i >>> 8) & 0xFF),
                    (byte) ((i >>> 16) & 0xFF),
                    (byte) ((i >>> 24) & 0xFF)
                };
    }
    
    public static byte[] longToByteArray(long l) {
        return new byte[]{
                    (byte) (l),
                    (byte) ((l >>> 8) & 0xFF),
                    (byte) ((l >>> 16) & 0xFF),
                    (byte) ((l >>> 24) & 0xFF),
                    (byte) ((l >>> 32) & 0xFF),
                    (byte) ((l >>> 40) & 0xFF),
                    (byte) ((l >>> 48) & 0xFF),
                    (byte) ((l >>> 56) & 0xFF)
                };
    }
}
