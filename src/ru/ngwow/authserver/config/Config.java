package ru.ngwow.authserver.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import ru.ngwow.authserver.exceptions.ConfigFileException;

public class Config {

    private Properties configFile;

    public Config(String file) throws IOException {
        if (file == null) {
            throw new NullPointerException("file is null");
        }
        configFile = new java.util.Properties();
        configFile.load(new FileInputStream(file));
    }

    public String getProperty(String key) throws ConfigFileException {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        String value = this.configFile.getProperty(key);
        if (value == null) {
            throw new ConfigFileException("Failed to get " + key + "property");
        }
        return value;
    }
}
