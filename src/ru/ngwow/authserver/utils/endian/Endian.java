package ru.ngwow.authserver.utils.endian;

public class Endian {
    
    public static short swapEndian(short a) {
        return (short)(((a & 0xff) << 8) | ((a & 0xff00) >>> 8));
    }
    
    public static int swapEndian(int a) {
        return ((a & 0xff) << 24) | ((a & 0xff00) << 8) | ((a & 0xff0000) >>> 8) | ((a & 0xff000000) >>> 24);
    }
}
