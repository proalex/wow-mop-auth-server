package ru.ngwow.authserver.database;

import java.io.IOException;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.impl.GenericObjectPoolFactory;
import ru.ngwow.authserver.exceptions.ConfigFileException;
import ru.ngwow.authserver.settings.AuthSettings;

public class MySqlConnection {

    private static volatile MySqlConnection instance;
    private static ObjectPool authPool;

    private MySqlConnection() throws IOException, ConfigFileException {
        String authHost = AuthSettings.get("mysql.auth.host");
        int authPort = AuthSettings.getInt("mysql.auth.port");
        String authSchema = AuthSettings.get("mysql.auth.schema");
        String authUser = AuthSettings.get("mysql.auth.user");
        String authPassword = AuthSettings.get("mysql.auth.password");
        int authThreads = AuthSettings.getInt("mysql.auth.threads");
        PoolableObjectFactory authFactory = new MySqlPoolableObjectFactory(authHost,
                authPort, authSchema, authUser, authPassword);
        GenericObjectPool.Config authPoolConfig = new GenericObjectPool.Config();
        authPoolConfig.maxActive = authThreads;
        authPoolConfig.testOnBorrow = true;
        authPoolConfig.testWhileIdle = true;
        authPoolConfig.timeBetweenEvictionRunsMillis = 10000;
        authPoolConfig.minEvictableIdleTimeMillis = 60000;
        GenericObjectPoolFactory authObjectPoolFactory =
                new GenericObjectPoolFactory(authFactory, authPoolConfig);
        authPool = authObjectPoolFactory.createPool();
    }

    public static MySqlConnection getInstance() throws IOException, ConfigFileException {
        MySqlConnection localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlConnection.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MySqlConnection();
                }
            }
        }
        return localInstance;
    }

    public ObjectPool getAuthPool() {
        return authPool;
    }
}
