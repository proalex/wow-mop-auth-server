# ************************************************************
# Sequel Pro SQL dump
# Версия 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: 192.168.1.140 (MySQL 5.5.29)
# Схема: auth
# Время создания: 2013-04-29 17:34:40 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы account_banned
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_banned`;

CREATE TABLE `account_banned` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `sessionkey` longtext,
  `online` tinyint(4) DEFAULT '0',
  `expansion` tinyint(4) unsigned NOT NULL DEFAULT '4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы characters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `characters`;

CREATE TABLE `characters` (
  `realmid` int(10) unsigned NOT NULL DEFAULT '0',
  `accountid` int(10) unsigned NOT NULL,
  `count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`accountid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы class_expansion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `class_expansion`;

CREATE TABLE `class_expansion` (
  `class` tinyint(3) NOT NULL,
  `expansion` tinyint(3) NOT NULL,
  PRIMARY KEY (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `class_expansion` WRITE;
/*!40000 ALTER TABLE `class_expansion` DISABLE KEYS */;

INSERT INTO `class_expansion` (`class`, `expansion`)
VALUES
	(1,0),
	(2,0),
	(3,0),
	(4,0),
	(5,0),
	(6,2),
	(7,0),
	(8,0),
	(9,0),
	(10,4),
	(11,0);

/*!40000 ALTER TABLE `class_expansion` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы race_expansion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `race_expansion`;

CREATE TABLE `race_expansion` (
  `race` tinyint(3) NOT NULL,
  `expansion` tinyint(3) NOT NULL,
  PRIMARY KEY (`race`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `race_expansion` WRITE;
/*!40000 ALTER TABLE `race_expansion` DISABLE KEYS */;

INSERT INTO `race_expansion` (`race`, `expansion`)
VALUES
	(1,0),
	(2,0),
	(3,0),
	(4,0),
	(5,0),
	(6,0),
	(7,0),
	(8,0),
	(9,3),
	(10,1),
	(11,1),
	(22,3),
	(24,4),
	(25,4),
	(26,4);

/*!40000 ALTER TABLE `race_expansion` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы realms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `realms`;

CREATE TABLE `realms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL,
  `port` smallint(5) unsigned NOT NULL,
  `icon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `timezone` tinyint(3) unsigned NOT NULL,
  `population` float unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
