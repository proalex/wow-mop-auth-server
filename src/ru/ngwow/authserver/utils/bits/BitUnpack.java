package ru.ngwow.authserver.utils.bits;

import java.io.EOFException;
import java.io.IOException;
import ru.ngwow.authserver.packet.Packet;
import ru.ngwow.authserver.utils.array.ArrayUtils;

public class BitUnpack {

    private Packet buffer;
    private short current;
    private int position;

    public BitUnpack(Packet buffer) {
        this.buffer = buffer;
    }
    
    synchronized public int read(final int count)
            throws IOException {
        if (count > 32 || count < 1) {
            throw new IOException("Invalid bit count.");
        }
        int value = 0;
        for (int i = count - 1; i >= 0; i--) {
            value = read() ? (1 << i) | value : value;
        }
        return value;
    }

    synchronized public boolean read() throws IOException {
        if (buffer == null) {
            throw new IOException("Already closed");
        }
        if (position == 0) {
            current = buffer.readByte();
            if (current == -1) {
                throw new EOFException();
            }
            position = 8;
        }
        return ((current >>> --position) & 1) != 0;
    }
    
    synchronized public long getPacketValue(byte[] mask, byte[] bytes) throws IOException {
        if (mask == null) {
            throw new NullPointerException("mask is null");
        }
        if (bytes == null) {
            throw new NullPointerException("bytes is null");
        }
        boolean[] valueMask = new boolean[mask.length];
        byte[] valueBytes = new byte[bytes.length];
        for (int i = 0; i < valueMask.length; i++) {
            valueMask[i] = read();
        }
        for (byte i = 0; i < bytes.length; i++) {
            if (valueMask[mask[i]]) {
                valueBytes[bytes[i]] = (byte) (buffer.readInt8() ^ 1);
            }
        }
        return ArrayUtils.byteArrayToLong(valueBytes);
    }
}
