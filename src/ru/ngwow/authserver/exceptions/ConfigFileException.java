package ru.ngwow.authserver.exceptions;

public class ConfigFileException extends Exception {

    public ConfigFileException(String msg) {
        super(msg);
    }
}
