package ru.ngwow.authserver.settings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import ru.ngwow.authserver.config.Config;
import ru.ngwow.authserver.exceptions.ConfigFileException;

public class AuthSettings {

    private static Map<String, String> settings = new HashMap<>();
    private static Config config;

    public static void load() throws IOException, ConfigFileException {
        if (config == null) {
            config = new Config("authserver.properties");
        }
        settings.clear();
        settings.put("network.ip", config.getProperty("network.ip"));
        settings.put("network.port", config.getProperty("network.port"));
        settings.put("mysql.auth.host", config.getProperty("mysql.auth.host"));
        settings.put("mysql.auth.port", config.getProperty("mysql.auth.port"));
        settings.put("mysql.auth.user", config.getProperty("mysql.auth.user"));
        settings.put("mysql.auth.password", config.getProperty("mysql.auth.password"));
        settings.put("mysql.auth.schema", config.getProperty("mysql.auth.schema"));
        settings.put("mysql.auth.threads", config.getProperty("mysql.auth.threads"));
        settings.put("thread.acceptor", config.getProperty("thread.acceptor"));
        settings.put("thread.io", config.getProperty("thread.io"));
        settings.put("thread.packethandler", config.getProperty("thread.packethandler"));
        settings.put("gamebuild", config.getProperty("gamebuild"));
    }

    public static String get(String key) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        return settings.get(key);
    }

    public static int getInt(String key) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        return new Integer(settings.get(key));
    }
}
