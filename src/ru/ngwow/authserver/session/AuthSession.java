package ru.ngwow.authserver.session;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.jboss.netty.channel.Channel;
import ru.ngwow.authserver.constants.AuthState;
import ru.ngwow.authserver.database.MySqlConnection;
import ru.ngwow.authserver.database.query.Query;
import ru.ngwow.authserver.utils.bignumber.BigNumber;

public final class AuthSession {

    private Channel channel;
    private String email;
    private int id;
    private String password;
    private BigNumber v;
    private BigNumber s;
    private BigNumber b;
    private BigNumber bSmall;
    private BigNumber reconnectRandom;
    private boolean authed;
    private boolean online;
    private BigNumber sessionKey;
    private static final BigNumber g = new BigNumber("7");
    private static final BigNumber n = new BigNumber("894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7");
    private AuthState state = AuthState.UNAUTHED;

    public AuthSession(Channel channel) {
        if (channel == null) {
            throw new NullPointerException("channel is null");
        }
        this.channel = channel;
        reconnectRandom = new BigNumber();
        reconnectRandom.setRand(16);
    }

    public boolean isAuthed() {
        return authed;
    }
    
    public void setAuthState(AuthState state) {
        if (state == null) {
            throw new NullPointerException("state is null");
        }
        this.state = state;
    }

    public void setSessionKey(BigNumber sessionKey)
            throws SQLException, Exception {
        if (sessionKey == null) {
            throw new NullPointerException("sessionKey is null");
        }
        this.sessionKey = sessionKey;
        this.authed = true;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statement;
            statement = connection.prepareStatement(Query.setSessionKey);
            statement.setString(1, sessionKey.asHexStr().toUpperCase());
            statement.setInt(2, id);
            statement.execute();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
    }

    public void setB(BigNumber b) {
        if (b == null) {
            throw new NullPointerException("b is null");
        }
        this.b = b;
    }

    public void setb(BigNumber b) {
        if (b == null) {
            throw new NullPointerException("b is null");
        }
        this.bSmall = b;
    }

    public BigNumber getReconnectRandom() {
        return reconnectRandom;
    }

    public BigNumber getb() {
        if (bSmall == null) {
            throw new NullPointerException("bSmall is null");
        }
        return bSmall;
    }

    public BigNumber getB() {
        if (b == null) {
            throw new NullPointerException("b is null");
        }
        return b;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getEmail() {
        if (email == null) {
            throw new NullPointerException("email is null");
        }
        return email;
    }

    public int getId() {
        if (id == 0) {
            throw new NullPointerException("id is null");
        }
        return id;
    }

    public void setAuthed(boolean authed) {
        this.authed = authed;
    }

    public void initVSFields() throws NoSuchAlgorithmException {
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        BigNumber I = new BigNumber(password);
        byte[] hash = I.asByteArray(20);
        int length = hash.length;
        for (int i = 0; i < (length / 2); i++) {
            byte j = hash[i];
            hash[i] = hash[length - 1 - i];
            hash[length - 1 - i] = j;
        }
        s = new BigNumber();
        s.setRand(32);
        sha.update(s.asByteArray());
        sha.update(hash);
        BigNumber x = new BigNumber();
        x.setBinary(sha.digest());
        v = g.modPow(x, n);
    }

    public BigNumber getV() {
        if (v == null) {
            throw new NullPointerException("v is null");
        }
        return v;
    }

    public BigNumber getS() {
        if (s == null) {
            throw new NullPointerException("s is null");
        }
        return s;
    }

    public BigNumber getSesionKey() {
        if (sessionKey == null) {
            throw new NullPointerException("sessionKey is null");
        }
        return sessionKey;
    }

    public boolean isBanned() throws SQLException, Exception {
        ResultSet bannedState;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getBannedState);
            statment.setInt(1, id);
            bannedState = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        if (!bannedState.next()) {
            throw new SQLException("Failed to get banned state.");
        }
        if (bannedState.getInt("COUNT(id)") > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isOnline() {
        return online;
    }

    public boolean getInfo(String email) throws SQLException, Exception {
        ResultSet accountInfo;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getAccountInfo);
            statment.setString(1, email);
            accountInfo = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        if (!accountInfo.next()) {
            return false;
        }
        this.email = email.toUpperCase();
        id = accountInfo.getInt("id");
        password = accountInfo.getString("password").toUpperCase();
        online = accountInfo.getInt("online") != 0 ? true : false;
        if (accountInfo.getString("sessionkey") != null
                && accountInfo.getString("sessionkey").length() == 80) {
            sessionKey = new BigNumber(accountInfo.getString("sessionkey").toUpperCase());
        }
        return true;
    }
    
    public AuthState getAuthState() {
        return state;
    }
}
