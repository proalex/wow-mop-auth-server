package ru.ngwow.authserver.utils.bits;

import java.io.IOException;
import java.util.BitSet;
import org.jboss.netty.buffer.ChannelBuffer;
import ru.ngwow.authserver.utils.array.ArrayUtils;

public class BitPack {

    private ChannelBuffer buffer;
    private int iBuffer;
    private int bitPos = 8;
    private byte[] guid;
    private byte[] guildGuid;
    
    public BitPack(ChannelBuffer buffer) {
        this.buffer = buffer;
    }
    
    public void setGuid(long guid) {
        this.guid = ArrayUtils.longToByteArray(guid);
    }
    
    public void setGuildGuid(long guildGuid) {
        this.guildGuid = ArrayUtils.longToByteArray(guildGuid);
    }
    
    public void writeGuildGuidMask(int... order) throws IOException {
        if (guildGuid == null) {
            throw new NullPointerException("guildGuid is null");
        }
        if (order == null) {
            throw new NullPointerException("order is null");
        }
        for (int current : order) {
            write(guildGuid[current] != 0 ? 1 : 0);
        }
    }
    
    public void writeGuidMask(int... order) throws IOException {
        if (guid == null) {
            throw new NullPointerException("guid is null");
        }
        if (order == null) {
            throw new NullPointerException("order is null");
        }
        for (int current : order) {
            write(guid[current] != 0 ? 1 : 0);
        }
    }
    
    public void writeGuildGuidBytes(int... order) throws IOException {
        if (guildGuid == null) {
            throw new NullPointerException("guildGuid is null");
        }
        if (order == null) {
            throw new NullPointerException("order is null");
        }
        for (int current : order) {
            if (guildGuid[current] != 0L) {
                write(guildGuid[current] ^ 1, 8);
            }
        }
    }
    
    public void writeGuidBytes(int... order) throws IOException {
        if (guid == null) {
            throw new NullPointerException("guid is null");
        }
        if (order == null) {
            throw new NullPointerException("order is null");
        }
        for (int current : order) {
            if (guid[current] != 0L) {
                write(guid[current] ^ 1, 8);
            }
        }
    }

    synchronized public void write(int bit) throws IOException {
        if (buffer == null) {
            throw new IOException("Already closed");
        }
        if (bit != 0 && bit != 1) {
            throw new IOException(bit + " is not a bit");
        }
        bitPos--;
        if (bit == 1) {
            iBuffer |= (1 << (bitPos));
        }
        if (bitPos == 0) {
            flush();
        }
    }
    
    synchronized public void write(boolean bit) throws IOException {
        write(bit ? 1 : 0);
    }

    public void flush() throws IOException {
        if (bitPos == 8) {
            return;
        }
        buffer.writeByte((byte) iBuffer);
        bitPos = 8;
        iBuffer = 0;
    }

    synchronized public void write(final int bits, final int aNumBits)
            throws IOException {
        for (int i = aNumBits - 1; i >= 0; i--) {
            write((bits >>> i) & 1);
        }
    }
    
    synchronized public void writeBitArray(BitSet bits, final int aNumBits)
            throws IOException {
        if (bits == null) {
            throw new NullPointerException("bits is null");
        }
        for (int i = aNumBits - 1; i >= 0; i--) {
            write(bits.get(i));
        }
    }
}